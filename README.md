<div align="center">
    <br />
    <br />
    <img width="300" height="auto" src="https://www.jianfv.top/image/quick_seal.png" />
    <h4>快捷的印章绘制JS插件</h4>
    <p align="center">
        <a href="https://www.npmjs.com/package/quick-seal">
        <img src="https://img.shields.io/badge/npm-v1.0.0-blue" alt="version" />
        </a>
        <a href="https://www.npmjs.com/package/quick-seal">
        <img src="https://img.shields.io/badge/License-MIT-yellow" />
        </a>
    </p>
</div>

#### 项目结构
```angular2html
|-- chat-area
    |
    |--fonts // 几款可以免费商用的字体包
    |
    |-- es5 // 低版本浏览器兼容
    |   |-- QuickSeal.js
    |   |-- System.js
    |
    |-- lib
    |    |-- QuickSeal.js
    |
    |-- demo.html // 示例页
```

### 安装
```shell
npm i --save quick-seal
```

### 基本使用
```javascript
import QuickSeal from 'quick-seal'

// 实例化对象
const seal = new QuickSeal({
    fontFamily: 'SimSun', // 字体名称
    text: '浙江某某网络科技有限公司', // 主体文案
    code: '1234567890123', // 圆形章防伪码
    horizontal: '某某专用章', // 横排字
    color: 'green', // 颜色
    need5Start: true, // 是否需要五角星
    innerLine: true, // 是否需要内圈
    outType: 'Base64', // 函数调用输出类型 'Base64' | 'Blob' | 'File'
    // outSize: '180,180' // 调整输出的图片大小 默认不调整
})

// 实例对象 修改配置项
seal.updateConfig({
    fontFamily: 'SimSun', // 字体名称
    text: '浙江某某网络科技有限公司', // 主体文案
    code: '1234567890123', // 圆形章防伪码
    horizontal: '某某专用章', // 横排字
    color: 'green', // 颜色
    need5Start: true, // 是否需要五角星
    innerLine: true, // 是否需要内圈
    outType: 'Base64', // 函数调用输出类型 'Base64' | 'Blob' | 'File'
    outSize: '180,180'
})

const onDraw = async () => {
    // 可加载外部字体包并使用该字体 (注*请确认你的项目对该字体拥有授权)
    await seal.loadFont({ url: 'fontUrl', name: 'fontName' })
    // 绘制圆形章
    const fileCircular = await seal.drawCircular()
    // 绘制椭圆章
    const fileEllipse = await seal.drawEllipse()
}
```

### 示例效果图
<div>
    <img width="auto" height="auto" src="https://www.jianfv.top/image/circular.png" />
    <img width="auto" height="auto" src="https://www.jianfv.top/image/ellipse.png" />
</div>